package com.ravendyne.todos.service;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigParameters {
  public static final String DEFAULT_DB_FILE_NAME = "todos-db.json";
  public Path dbPath;
  public Integer port;
  public ConfigParameters() {
    dbPath = Paths.get( DEFAULT_DB_FILE_NAME );
    port = 0;
  }
}
