package com.ravendyne.todos.service;

import picocli.CommandLine.Option;

public interface CliParameters {
  @Option(names = "--db", defaultValue = "")
  String getDbPath();
  @Option(names = "--port", defaultValue = "0")
  String getPort();
}
