// SPDX-License-Identifier: MIT
// (C) 2022 - Ravendyne Inc
package com.ravendyne.todos.service;

import java.net.URL;
import java.io.IOException;


final class SystemStart {
  private SystemStart() {}

  public static void open( URL url ) throws IOException {

    String os = System.getProperty("os.name").toLowerCase();
    Runtime rt = Runtime.getRuntime();

    // Windows:
    if( os.indexOf("win") >= 0 ) {
      // When the FIRST parameter has quotes, it is taken as the title of the CMD window.
      // That's why `start "" thingie` runs the thingie, but `start "thingie"` runs CMD
      rt.exec("start \"\" \"" + url.toString() +"\"");
    } else

    // Mac:
    if( os.indexOf("mac") >= 0 ) {
      rt.exec("open \"" + url.toString() + "\"");
    } else

    // Linux:
    if( os.indexOf("nix") >=0 || os.indexOf("nux") >=0 ) {
      rt.exec("xdg-open \"" + url.toString() + "\"");
    }
  }
}
