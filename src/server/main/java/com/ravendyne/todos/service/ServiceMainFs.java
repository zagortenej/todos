// SPDX-License-Identifier: MIT
// (C) 2022 - Ravendyne Inc
package com.ravendyne.todos.service;

import java.nio.file.Paths;

import org.eclipse.jetty.server.Handler;

import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.PathResource;


public class ServiceMainFs extends ServiceBase {

  public static void main( String[] args ) throws Exception {
    ServiceMainFs service = new ServiceMainFs( args );
    service.serve();
  }

  public ServiceMainFs( String[] args ) {
    super( args );
  }

  @Override
  protected Handler getStaticContentHandler() {
    ResourceHandler resourceHandler = new ResourceHandler();
    // resourceHandler.setDirectoriesListed( true );
    // ONLY use path info to construct resource location,
    // don't use servlet path or 'javax.servlet.include.servlet_path' request attribute
    resourceHandler.setPathInfoOnly( true );
    resourceHandler.setWelcomeFiles( new String[] { "index.html" } );
    // System.out.println(System.getProperty("user.dir"));
    // https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html
    resourceHandler.setBaseResource( new PathResource( Paths.get( System.getProperty("user.dir"), "dist/todos-app" ) ) );
    return resourceHandler;
  }
}
