// SPDX-License-Identifier: MIT
// (C) 2022 - Ravendyne Inc
package com.ravendyne.todos.service;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.Resource;


public class ServiceMainBundled extends ServiceBase {

  public static void main( String[] args ) throws Exception {
    ServiceMainBundled service = new ServiceMainBundled( args );
    service.serve();
  }

  public ServiceMainBundled( String[] args ) {
    super( args );
  }

  @Override
  protected Handler getStaticContentHandler() {
    ResourceHandler resourceHandler = new ResourceHandler();
    // resourceHandler.setDirectoriesListed( true );
    resourceHandler.setPathInfoOnly( true );
    resourceHandler.setWelcomeFiles( new String[] { "index.html" } );
    resourceHandler.setBaseResource( Resource.newClassPathResource( "webui" ) );
    return resourceHandler;
  }
}
