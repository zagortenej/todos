// SPDX-License-Identifier: MIT
// (C) 2022 - Ravendyne Inc
package com.ravendyne.todos.service.api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ravendyne.todos.service.ServiceBase;


@Path("todos")
public class RestApiTodosDb {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getDb() {

    String content = "{}";
    try {
      if( !Files.exists( ServiceBase.configParams.dbPath ) ) {
        Files.writeString( ServiceBase.configParams.dbPath, "{}", StandardCharsets.UTF_8 );
      }
      content = Files.readString( ServiceBase.configParams.dbPath );
    } catch( IOException ex ) {
      ex.printStackTrace();
    }

    // System.out.println(">>>> getDb");
    // System.out.println(">>>> " + content);

    return content;
  }

  @POST
  @Consumes(MediaType.TEXT_PLAIN)
  @Produces(MediaType.TEXT_PLAIN)
  public String saveDb( String content ) {

    // System.out.println(">>>> saveDb");
    // System.out.println(">>>> " + content);

    try {
      Files.writeString( ServiceBase.configParams.dbPath, content, StandardCharsets.UTF_8 );
    } catch( IOException ex ) {
      ex.printStackTrace();
    }

    return "{}";
  }
}
