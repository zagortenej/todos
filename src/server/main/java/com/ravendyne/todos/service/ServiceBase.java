// SPDX-License-Identifier: MIT
// (C) 2022 - Ravendyne Inc
package com.ravendyne.todos.service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.DispatcherType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.NetworkConnector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.rewrite.handler.RewriteRegexRule;
import org.eclipse.jetty.rewrite.handler.RuleContainer;
import org.glassfish.jersey.servlet.ServletContainer;

import com.ravendyne.todos.service.api.RestApiPackage;

import picocli.CommandLine;


public abstract class ServiceBase {

  public static ConfigParameters configParams = new ConfigParameters();

  private void parseCliParameters( String[] args ) {
    configParams.port = Integer.parseInt( System.getProperty( "service.port", "0" ) );
    configParams.dbPath = Paths.get( System.getProperty( "service.db", ConfigParameters.DEFAULT_DB_FILE_NAME ) );
    // configParams.dbPath = Paths.get( System.getProperty("user.dir"), ConfigParameters.DEFAULT_DB_FILE_NAME );

    CommandLine cmd = new CommandLine( CliParameters.class );
    cmd.parseArgs( args );
    CliParameters cliParams = cmd.getCommand();

    if( ! cliParams.getDbPath().isEmpty() ) {
      configParams.dbPath = Paths.get( cliParams.getDbPath() );
    }
    try {
      Integer port = Integer.parseUnsignedInt( cliParams.getPort(), 0 );
      if( port != 0 ) configParams.port = port;
    } catch( NumberFormatException ex ) {
    }

    System.out.println("DB: " + configParams.dbPath);
    System.out.println("PORT: " + configParams.port);
  }

  public ServiceBase( String[] args ) {
    parseCliParameters( args );
  }

  public void serve() throws Exception {

    boolean localhostOnly = true;

    Server server = createServer( configParams.port, localhostOnly );

    try {
      server.start();
      System.out.println(
        String.format( "Started server at: http://%s:%d", localhostOnly ? "localhost" : "0.0.0.0",
        getJettyPort( server, configParams.port ) )
      );

      server.join();

    } catch (Exception ex) {
      Logger.getLogger( ServiceBase.class.getName() ).log( Level.SEVERE, null, ex );
    } finally {
      server.destroy();
    }
  }

  private int getJettyPort( Server server, int defaultPort ) {
    int jettyPort = defaultPort;
    for( Connector connector : server.getConnectors() ) {
      if( connector instanceof NetworkConnector ) {
        // https://www.eclipse.org/jetty/javadoc/jetty-9/org/eclipse/jetty/server/NetworkConnector.html
        jettyPort = ((NetworkConnector) connector).getLocalPort();
        break;
      }
    }
    return jettyPort;
  }

  private Server createServer( int port, boolean localhostOnly ) throws Exception {

    Server server;
    if( localhostOnly ) {
      server = new Server( new java.net.InetSocketAddress( "localhost", port ) );
      // Server server = new Server(new
      // java.net.InetSocketAddress(java.net.InetAddress.getByAddress(new
      // byte[]{127,0,0,1}),0));
    } else {
      server = new Server( port );
    }

    // add all handlers to the server
    HandlerList handlers = new HandlerList();
    handlers.setHandlers( new Handler[] {
      rewriteForAngular( getStaticContentHandler() ),
      getRestApiHandler(),
      new DefaultHandler()
    });
    server.setHandler( handlers );

    return server;
  }

  static class MyRH extends RewriteHandler {

    RewriteRegexRule html5pushState;

    public MyRH() {
      super();
      html5pushState = new RewriteRegexRule();
      // html5pushState.setRegex("/.*");
      html5pushState.setRegex("(^(?!/api).*)"); // regex MUST have at least one matching group
      html5pushState.setReplacement("/index.html");

      Log.getLogger(RuleContainer.class).setDebugEnabled(true);
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

      System.out.println("MyRH: dispatcherTypes = " + getDispatcherTypes());
      System.out.println("MyRH: baseRequest.dispatcherType = " + baseRequest.getDispatcherType());
      System.out.println("MyRH: target = " + target);
      System.out.println("MyRH: RequestURI = " + request.getRequestURI());
      System.out.println("myRH: rrr = " + html5pushState.matchAndApply(target, request, response));
      System.out.println();
      super.handle(target, baseRequest, request, response);
    }
  }

  protected Handler rewriteForAngular( Handler staticContentHandler ) {
    // Enable URL Rewriting to support HTML 5 PushState
    // https://www.eclipse.org/jetty/javadoc/jetty-9/org/eclipse/jetty/rewrite/handler/RewriteHandler.html
    // RewriteHandler urlRewriteHandler = new MyRH();
    RewriteHandler urlRewriteHandler = new RewriteHandler();
    // see RuleContainer.apply() for what these two do
    urlRewriteHandler.setRewriteRequestURI(true);
    urlRewriteHandler.setRewritePathInfo(true); // <- MUST be set if staticContentHandler is ResourceHandler
    //
    urlRewriteHandler.setOriginalPathAttribute("requestedPath");

    // https://www.eclipse.org/jetty/javadoc/jetty-9/org/eclipse/jetty/rewrite/handler/RewriteRegexRule.html
    RewriteRegexRule html5pushState = new RewriteRegexRule();
    // if regex matches, the setReplacement() value is used as replacement,
    // and any $N in the replacement is replaced by N-th matched group.
    // the replacement will be used for baseRequest.setURIPathQuery() if setRewriteRequestURI() is true
    // the replacement will also be used for baseRequest.setPathInfo() if setRewritePathInfo() is true
    // ResourceHandler() uses baseRequest.getPathInfo()

    // html5pushState.setRegex("/.*");
    // html5pushState.setRegex("(^(?!/api).*)");
    // html5pushState.setRegex("(^/dash)");
    html5pushState.setRegex("^/dash.*");
    html5pushState.setReplacement("/index.html");
    // html5pushState.setHandling(true); // <- only if this rule handles request, i.e. sends response back or whatever
    urlRewriteHandler.addRule(html5pushState);

    // Handler Structure: UrlRewriteHandler will filter URLs before they reach the webapp context.
    urlRewriteHandler.setHandler(staticContentHandler);

    return urlRewriteHandler;
  }

  protected Handler getRestApiHandler() {
    ServletContextHandler ctx = new ServletContextHandler( ServletContextHandler.NO_SESSIONS );
    ctx.setContextPath( "/" );

    ServletHolder serHol = ctx.addServlet( ServletContainer.class, "/api/*" );
    serHol.setInitOrder( 1 );
    serHol.setInitParameter( "jersey.config.server.provider.packages", RestApiPackage.class.getPackage().getName() );

    // Use this...
    // FilterHolder holder = new FilterHolder( CrossOriginFilter.class );
    // holder.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
    // holder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,OPTIONS");
    // holder.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin");
    // // holder.setInitParameter( CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*"); // "Access-Control-Allow-Origin"
    // // holder.setInitParameter( CrossOriginFilter.ACCESS_CONTROL_ALLOW_METHODS_HEADER, "GET,POST,OPTIONS"); // "Access-Control-Allow-Methods"
    // // holder.setInitParameter( CrossOriginFilter.ACCESS_CONTROL_ALLOW_HEADERS_HEADER, "X-Requested-With,Content-Type,Accept,Origin"); // "Access-Control-Allow-Headers"
    // holder.setName("cross-origin");
    // ctx.addFilter( holder, "/api/*", EnumSet.of( DispatcherType.REQUEST ) );

    // ...or this for CORS filter
    FilterHolder cors = ctx.addFilter( CrossOriginFilter.class, "/api/*", EnumSet.of( DispatcherType.REQUEST ) );
    // cors.setInitParameter( CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*"); // "Access-Control-Allow-Origin"
    // cors.setInitParameter( CrossOriginFilter.ACCESS_CONTROL_ALLOW_METHODS_HEADER, "GET,POST,OPTIONS"); // "Access-Control-Allow-Methods"
    // cors.setInitParameter( CrossOriginFilter.ACCESS_CONTROL_ALLOW_HEADERS_HEADER, "X-Requested-With,Content-Type,Accept,Origin"); // "Access-Control-Allow-Headers"
    cors.setInitParameter( CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*"); // "allowedOrigins"
    cors.setInitParameter( CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,OPTIONS"); // "allowedMethods"
    cors.setInitParameter( CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin"); // "allowedHeaders"

    return ctx;
  }

  protected abstract Handler getStaticContentHandler();
}
