// SPDX-License-Identifier: MIT
// (C) 2022 - Ravendyne Inc
package com.ravendyne.todos.service.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("msg")
public class RestApiMsg {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getMessage() {

    return "My message\n";
  }

  @Path("/{name}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public User hello(@PathParam("name") String name) {

    User obj = new User();
    obj.setId(0);
    obj.setName(name);

    return obj;

  }

  @Path("/all")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<User> helloList() {

    List<User> list = new ArrayList<>();

    User obj1 = new User();
    obj1.setId(1);
    obj1.setName("mkyong");
    list.add(obj1);

    User obj2 = new User();
    obj2.setId(2);
    obj2.setName("zilap");
    list.add(obj2);

    return list;

  }

  private static final ObjectMapper mapper = new ObjectMapper();

  @Path("/create")
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response create(User user) {

    ObjectNode json = mapper.createObjectNode();
    json.put("status", "ok");
    return Response.status(Response.Status.CREATED).entity(json).build();

  }
}
