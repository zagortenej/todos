import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CardType } from 'src/app/service/card-data.model';

export interface CardEditDialogData {
  readOnly?: boolean;
  dialogTitle?: string;
  cardId: number;
  text: string;
  details?: string;
  type: string;
  cardTypes: CardType[];
}

export interface CardEditDialogResult {
  cardId: number;
  text: string;
  details?: string;
  type: string;
}

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-card-dialog.component.html',
  styleUrls: ['./edit-card-dialog.component.scss']
})
export class EditCardDialogComponent {

  dialogTitle: string = 'Card info';

  readOnly: boolean = true;

  result: CardEditDialogResult;

  get resultTypeColour(): string { return this.cardTypes.find( ct => ct.label == this.result.type )?.colour || '' }

  showPreview: boolean = false;

  cardTypes: CardType[];

  constructor(
    public dialogRef: MatDialogRef<EditCardDialogComponent, CardEditDialogResult>,
    @Inject(MAT_DIALOG_DATA) data: CardEditDialogData,
  ) {
    this.dialogTitle = data.dialogTitle || this.dialogTitle;
    this.cardTypes = data.cardTypes;
    this.result = {
      cardId: data.cardId,
      text: data.text,
      details: data.details,
      type: data.type,
    };
    this.readOnly = !!data.readOnly;
  }

  onEnterKey() {
    this.dialogRef.close( this.result );
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
