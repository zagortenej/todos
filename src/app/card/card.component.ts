import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as _ from 'lodash-es';

import { CardData, CardType } from '../service/card-data.model';
import { CardsService, DEFAULT_CARD_TYPE } from '../service/cards.service';
import { CardEditDialogData, CardEditDialogResult, EditCardDialogComponent } from './edit-card-dialog/edit-card-dialog.component';
import { confirmDialog } from '../components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() data: CardData = {id:0,type:'',text:'',done:false};

  get detailsColour() : string { return this.data?.details ? 'warn' : 'primary' };

  get typeColour() : string {
    const ct = this.cardsService.getCardType( this.data.type ) || this.defaultCardType;
    return ct.colour;
  };

  private defaultCardType: CardType = _.cloneDeep( DEFAULT_CARD_TYPE );

  constructor(
    private dialog: MatDialog,
    private cardsService: CardsService,
  ) {
    this.cardsService.onDataChanged.subscribe( () => {
      this.defaultCardType = this.cardsService.getCardType('default') || _.cloneDeep( DEFAULT_CARD_TYPE );
    })
  }

  cardDone() {
    // console.log('Card is done', this.data);
    this.cardsService.markAsDone( this.data.id );
  }

  cardDeleted() {
    // console.log('Card is deleted', this.data);
    confirmDialog( this.dialog, 'Delete card', 'Are you sure?', () => {
      this.cardsService.deleteCard( this.data.id );
    })
  }

  cardCancelled() {
    // console.log('Card is cancelled', this.data);
  }

  cardEdit() {
    const dialogRef = this.dialog.open<EditCardDialogComponent, CardEditDialogData, CardEditDialogResult>(EditCardDialogComponent, {
      minWidth: 400,
      data: {
        cardId: this.data.id,
        text: this.data.text,
        details: this.data.details,
        cardTypes: this.cardsService.getCardTypes(),
        type: this.data.type || DEFAULT_CARD_TYPE.label,
      },
    });

    dialogRef.afterClosed().subscribe( result => {
      if( result ) {
        // console.log('Card is edited', this.data);
        this.cardsService.updateCard( result.cardId, result.text, result.details, result.type );
      }
    });
  }

  cardViewDescription() {
    this.dialog.open<EditCardDialogComponent, CardEditDialogData, CardEditDialogResult>(EditCardDialogComponent, {
      minWidth: 400,
      data: {
        readOnly: true,
        cardId: this.data.id,
        text: this.data.text,
        details: this.data.details,
        cardTypes: this.cardsService.getCardTypes(),
        type: this.data.type,
      },
    });
  }
}
