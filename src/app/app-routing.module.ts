import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashComponent } from './dash/dash.component';
import { CdkDragDropHandleExampleComponent } from './examples/cdk-drag-drop-handle-example/cdk-drag-drop-handle-example.component';

const routes: Routes = [
  { path: 'example-1', component: CdkDragDropHandleExampleComponent },
  { path: 'dash', component: DashComponent },
  { path: '', redirectTo: '/dash', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
