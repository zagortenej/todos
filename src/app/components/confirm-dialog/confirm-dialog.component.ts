import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

export interface ConfirmDialogData {
  title: string;
  message: string;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {

  result: boolean;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent, boolean>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData,
  ) {
    this.result = false;
  }

  onCancel(): void {
    this.dialogRef.close( false );
  }

  onYes(): void {
    this.dialogRef.close( true );
  }

}

export const confirmDialog = ( dialog: MatDialog, title: string, message: string, confirmed: () => void ) => {
  const dialogRef = dialog.open<ConfirmDialogComponent, ConfirmDialogData, boolean>(ConfirmDialogComponent, {
    data: {
      title: title,
      message: message,
    },
  });

  dialogRef.afterClosed().subscribe( result => {
    if( result ) confirmed();
  });
}
