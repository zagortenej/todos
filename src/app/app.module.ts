import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashComponent } from './dash/dash.component';
import { CardComponent } from './card/card.component';
import { CdkDragDropHandleExampleComponent } from './examples/cdk-drag-drop-handle-example/cdk-drag-drop-handle-example.component';
import { EditCardDialogComponent } from './card/edit-card-dialog/edit-card-dialog.component';
import { ImportDialogComponent } from './dash/import-dialog/import-dialog.component';
import { SelectImportFileComponent } from './dash/import-dialog/select-import-file/select-import-file.component';
import { EditLaneDialogComponent } from './dash/edit-lane-dialog/edit-lane-dialog.component';
import { EditCollectionDialogComponent } from './dash/edit-collection-dialog/edit-collection-dialog.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

import { MarkdownModule } from 'ngx-markdown';

@NgModule({
  declarations: [
    AppComponent,
    DashComponent,
    CardComponent,
    EditCardDialogComponent,
    CdkDragDropHandleExampleComponent,
    ImportDialogComponent,
    SelectImportFileComponent,
    EditLaneDialogComponent,
    EditCollectionDialogComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    FormsModule,
    HttpClientModule,

    AppRoutingModule,

    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatSelectModule,

    MarkdownModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
