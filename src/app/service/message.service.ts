import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private snackBar: MatSnackBar,
  ) {}

  notify( message: string, action?: string ) {
    this.snackBar.open(message, action || 'OK', { duration: 5000 });
  }

}
