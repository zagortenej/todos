import { EventEmitter, Injectable } from '@angular/core';
import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import * as _ from 'lodash-es';

import { CardData, CardLane, CardType, LaneSet } from './card-data.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { BackendService } from './backend.service';
import { MessageService } from './message.service';

export const DEFAULT_CARD_TYPE: CardType = { label: 'default', colour: 'lightgreen' };

const DEFAULT_CARD_TYPES: CardType[] = [
  { label: 'simple', colour: 'yellow' },
  { label: 'complex', colour: 'red' },
  { label: 'sortof', colour: 'blue' },
  _.cloneDeep( DEFAULT_CARD_TYPE ),
]

export const EMPTY_COLLECTION: LaneSet = {
  title: 'empty collection',
  cardTypes: _.cloneDeep( DEFAULT_CARD_TYPES ),
  nextId: 1,
  lanes: [],
}


@Injectable({
  providedIn: 'root'
})
export class CardsService {

  private dataSet: LaneSet;

  private dataChanged = new BehaviorSubject<CardLane[]>([]);
  get onDataChanged(): Observable<CardLane[]> { return this.dataChanged.asObservable(); }
  // private triggerOnDataChanged() { this.dataChanged.next( this.dataSet.lanes ); }
  private triggerOnDataChanged() { this.dataChanged.next( _.cloneDeep(this.dataSet.lanes) ); }

  private dataSaved = new EventEmitter<void>();
  get onDataSaved(): Observable<void> { return this.dataSaved.asObservable(); }

  get title(): string { return this.dataSet.title; }
  set title( value: string ) { this.dataSet.title = value; }

  constructor(
    private backendService: BackendService,
    private messageService: MessageService,
  ) {
    this.dataSet = _.cloneDeep( EMPTY_COLLECTION );
    this.triggerOnDataChanged();
  }

  static isValidLaneSet( collection: LaneSet ): boolean {
    return !!collection.title && !!collection.nextId && !!collection.lanes;
  }

  getFromBackend() {
    this.backendService.getDb().subscribe( content => {
      console.log( 'getFromBackend', content );
      const collection: LaneSet = JSON.parse( content );
      if( CardsService.isValidLaneSet( collection ) ) {
        this.import( collection );
      } else {
        console.error( 'backend response is not a valid collection' );
        this.import( _.cloneDeep( EMPTY_COLLECTION ) );
      }
    })
  }

  pushToBackend() {
    return this.backendService.persistDb( JSON.stringify( this.export(), null, 2 ) ).subscribe( _ => {
      this.messageService.notify( 'Saved' );
      this.dataSaved.emit();
    });
  }

  export(): LaneSet {
    return _.cloneDeep( this.dataSet );
  }

  import( collection: LaneSet ) {
    this.dataSet = _.cloneDeep( collection );
    if( ! this.dataSet.cardTypes ) this.dataSet.cardTypes = _.cloneDeep( DEFAULT_CARD_TYPES );
    if( ! this.getCardType( 'default' ) ) this.dataSet.cardTypes.push( DEFAULT_CARD_TYPE );
    this.triggerOnDataChanged();
  }



  private findLane( laneId: number ): CardLane | undefined {
    return this.dataSet.lanes.find( lane => laneId == lane.id );
  }

  moveLane( previousIndex: number, currentIndex: number ) {
    moveItemInArray( this.dataSet.lanes, previousIndex, currentIndex );
    this.triggerOnDataChanged();
  }

  updateLane( laneId: number, title: string, type?: string ) {
    for( let lane of this.dataSet.lanes ) {
      if( laneId == lane.id ) {
        lane.title = title;
        // if( type ) lane.type = type;
        this.triggerOnDataChanged();
        return;
      }
    }
  }

  newLane( title: string ): CardLane {
    let lane: CardLane = {
      id: this.dataSet.nextId,
      title: title,
      cards: [],
    }
    this.dataSet.nextId++;

    this.dataSet.lanes.push( lane );

    this.triggerOnDataChanged();

    return _.cloneDeep( lane );
  }

  deleteLane( laneId: number ): CardLane | undefined {
    // console.log('deleteLane',laneId)

    for( let idx = 0; idx < this.dataSet.lanes.length; idx++ ) {
      let lane = this.dataSet.lanes[ idx ];
      // console.log('deleteLane lane',lane)
      if( laneId == lane.id ) {
        // console.log('deleteLane yes',lane)
        this.dataSet.lanes.splice( idx, 1 );
        this.triggerOnDataChanged();
        return lane;
      }
    }

    return undefined;
  }

  setLaneTitle( laneId: number, title: string ) {
    let lane = this.findLane( laneId );
    if( ! lane ) return;

    lane.title = title;
    this.triggerOnDataChanged();
  }



  private findCard( cardId: number ): CardData | undefined {
    for( let lane of this.dataSet.lanes ) {
      const card = lane.cards.find( card => cardId == card.id );
      if( card ) return card;
    }
    return undefined;
  }

  moveCardInLane( laneId: number, previousIndex: number, currentIndex: number ) {
    let lane: CardLane | undefined = this.findLane( laneId );
    if( !lane ) throw `No card lane with ID ${laneId}`;
    moveItemInArray( lane.cards, previousIndex, currentIndex );
    this.triggerOnDataChanged();
  }

  moveCardBetweenLanes( previousLaneId: number, laneId: number, previousIndex: number, currentIndex: number ) {
    let previousLane: CardLane | undefined = this.findLane( previousLaneId );
    if( !previousLane ) throw `No card lane with ID ${previousLaneId}`;
    let lane: CardLane | undefined = this.findLane( laneId );
    if( !lane ) throw `No card lane with ID ${laneId}`;

    transferArrayItem(
      previousLane.cards,
      lane.cards,
      previousIndex,
      currentIndex,
    );
    this.triggerOnDataChanged();
  }

  newCard( laneId: number, text: string, details?: string, type?: string  ): CardData | undefined {

    let lane = this.dataSet.lanes.find( lane => laneId == lane.id );
    if( ! lane ) return undefined;

    let card: CardData = {
      id: this.dataSet.nextId,
      text: text,
      details: details,
      type: type || DEFAULT_CARD_TYPE.label,
      done: false,
    }
    this.dataSet.nextId++;

    lane.cards.unshift( card );

    this.triggerOnDataChanged();

    return _.cloneDeep( card );
  }

  updateCard( cardId: number, text: string, details?: string, type?: string ) {
    const card = this.findCard( cardId );
    if( card ) {
      card.text = text;
      if( type ) card.type = type;
      if( details ) card.details = details;
      this.triggerOnDataChanged();
    }
  }

  markAsDone( cardId: number ) {
    const card = this.findCard( cardId );
    if( card ) {
      // console.log('markAsDone', card)
      card.done = true;
      this.triggerOnDataChanged();
    }
  }

  deleteCard( cardId: number ): CardData | undefined {
    for( let lane of this.dataSet.lanes ) {
      for( let idx = 0; idx < lane.cards.length; idx ++ ) {
        let card = lane.cards[idx];
        if( cardId == card.id ) {
          lane.cards.splice( idx, 1 );
          this.triggerOnDataChanged();
          return card;
        }
      }
    }
    return undefined;
  }

  deleteCards( cardIds: number[] ): void {
    if( cardIds.length == 0 ) return;
    let hasDeleted = false;
    for( const lane of this.dataSet.lanes ) {
      const cardsToLeave = lane.cards.filter( cd => ! cardIds.includes( cd.id ) );
      hasDeleted = hasDeleted || cardsToLeave.length != lane.cards.length;
      // empty the array
      lane.cards.splice(0);
      // put back the ones that are ok
      lane.cards.push(...cardsToLeave);
    }
    if( hasDeleted ) this.triggerOnDataChanged();
  }

  getCardTypes(): CardType[] {
    return _.cloneDeep( this.dataSet.cardTypes || DEFAULT_CARD_TYPES );
  }

  getCardType( label: string ): CardType | undefined {
    const ct = (this.dataSet.cardTypes || DEFAULT_CARD_TYPES ).find( ct => ct.label === label );
    if( ! ct ) return undefined;
    return _.cloneDeep( ct );
  }
}
