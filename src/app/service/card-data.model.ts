
export interface CardData {
  id: number;
  type: string;
  text: string;
  details?: string;
  done: boolean;
}

export interface CardType {
  label: string;
  colour: string;
}

export interface CardLane {
  id: number;
  title: string;
  cards: CardData[];
}

export interface LaneSet {
  lanes: CardLane[];
  cardTypes?: CardType[];
  nextId: number;
  title: string;
}
