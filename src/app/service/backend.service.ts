import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, of } from 'rxjs';

type Todos = any;

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private todosUrl = 'api/todos';

  private httpOptions = {
    // headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    headers: new HttpHeaders({ 'Content-Type': 'text/plain' })
  };

  constructor(
    private http: HttpClient,
  ) { }

  getDb(): Observable<string> {
    return this.http.get( this.todosUrl, { responseType: 'text' } )
    .pipe(
      catchError( this.handleError<Todos>( 'getDb', [] ) )
    );
  }

  persistDb( content: string ): Observable<string> {
    return this.http.post<string>( this.todosUrl, content, this.httpOptions ).pipe(
      catchError( this.handleError<Todos>('persistDb') )
    );
  }





  getTodos(): Observable<Todos> {
    return this.http.get<Todos>( this.todosUrl )
    .pipe(
      catchError( this.handleError<Todos>( 'getTodos', [] ) )
    );
  }

  updateTodos( todos: Todos ): Observable<any> {
    return this.http.put( this.todosUrl, todos, this.httpOptions ).pipe(
      // tap(_ => this.log(`updated todos id=${todos.id}`)),
      catchError( this.handleError<any>( 'updateTodos' ) )
    );
  }

  createTodos( todos: Todos ): Observable<Todos> {
    return this.http.post<Todos>( this.todosUrl, todos, this.httpOptions ).pipe(
      // tap((newTodos: Todos) => this.log(`added todos w/ id=${newTodos.id}`)),
      catchError( this.handleError<Todos>('addTodos') )
    );
  }

  deleteTodos( id: number ): Observable<Todos> {
    const url = `${this.todosUrl}/${id}`;

    return this.http.delete<Todos>(url, this.httpOptions).pipe(
      // tap(_ => this.log(`deleted todos id=${id}`)),
      catchError( this.handleError<Todos>('deleteTodos') )
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
