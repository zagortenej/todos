import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface EditCollectionDialogData {
  title: string;
}

export interface EditCollectionDialogResult {
  title: string;
}

@Component({
  selector: 'app-edit-collection-dialog',
  templateUrl: './edit-collection-dialog.component.html',
  styleUrls: ['./edit-collection-dialog.component.scss']
})
export class EditCollectionDialogComponent {

  result: EditCollectionDialogResult = {
    title: '',
  };

  constructor(
    public dialogRef: MatDialogRef<EditCollectionDialogComponent, EditCollectionDialogResult>,
    @Inject(MAT_DIALOG_DATA) data: EditCollectionDialogData,
  ) {
    this.result = {
      title: data.title,
    };
  }

  onEnterKey() {
    this.dialogRef.close( this.result );
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
