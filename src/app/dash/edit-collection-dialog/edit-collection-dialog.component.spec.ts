import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCollectionDialogComponent } from './edit-collection-dialog.component';

describe('EditCollectionDialogComponent', () => {
  let component: EditCollectionDialogComponent;
  let fixture: ComponentFixture<EditCollectionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCollectionDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditCollectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
