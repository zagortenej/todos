import { Component, OnInit } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { CardLane } from '../service/card-data.model';
import { CardsService } from '../service/cards.service';
import { MatDialog } from '@angular/material/dialog';
import { ImportDialogComponent, ImportDialogData, ImportDialogResult } from './import-dialog/import-dialog.component';
import { EditLaneDialogComponent, EditLaneDialogData, EditLaneDialogResult } from './edit-lane-dialog/edit-lane-dialog.component';
import { EditCollectionDialogComponent, EditCollectionDialogData, EditCollectionDialogResult } from './edit-collection-dialog/edit-collection-dialog.component';
import { confirmDialog } from '../components/confirm-dialog/confirm-dialog.component';
import { CardEditDialogData, CardEditDialogResult, EditCardDialogComponent } from '../card/edit-card-dialog/edit-card-dialog.component';

import { version } from 'src/app/version'

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit {

  version = version;

  lanes: CardLane[] = [];
  setTitle: string = '';

  hasUnsavedChanges = false;
  private initialLoad = true;

  constructor(
    private dialog: MatDialog,
    private cardsService: CardsService
  ) {}

  ngOnInit() {
    this.cardsService.onDataChanged.subscribe( data => {
      if( data.length == 0 && this.lanes.length == 0 ) return;

      this.lanes = data;
      this.setTitle = this.cardsService.title;
      if( this.initialLoad ) {
        this.initialLoad = false;
        return;
      }
      this.hasUnsavedChanges = true;
    })

    this.cardsService.onDataSaved.subscribe( () => {
      this.hasUnsavedChanges = false;
    });

    this.cardsService.getFromBackend();
  }

  editCollection() {
    const dialogRef = this.dialog.open<EditCollectionDialogComponent, EditCollectionDialogData, EditCollectionDialogResult>(EditCollectionDialogComponent, {
      data: { title: this.setTitle },
    });

    dialogRef.afterClosed().subscribe( result => {
      if( result ) {
        this.cardsService.title = result.title;
        this.setTitle = this.cardsService.title;
        console.log('Collection is edited', result );
      }
    });
  }

  addLane() {
    const dialogRef = this.dialog.open<EditLaneDialogComponent, EditLaneDialogData, EditLaneDialogResult>(EditLaneDialogComponent, {
      data: {
        dialogTitle: 'New lane',
        laneId: 0,
        title: 'new lane title',
      },
    });

    dialogRef.afterClosed().subscribe( result => {
      if( result ) {
        this.cardsService.newLane( result.title );
        console.log('Lane is added', result );
      }
    });
  }

  startNewCollection() {
    confirmDialog( this.dialog, 'Start new collection', `This will delete ALL cards and ALL lanes from current collection.Are you sure?`, () => {
      this.cardsService.import({
        nextId: 1,
        title: 'New todos',
        lanes: [],
      })
    })
  }

  removeLane( lane: CardLane ) {
    // console.log('removeLane',lane.id)
    confirmDialog( this.dialog, `Delete lane "${lane.title}"`, `Are you sure?`, () => {
      this.cardsService.deleteLane( lane.id );
    })
  }

  cleanupLane( lane: CardLane ) {
    confirmDialog( this.dialog, `Cleanup lane "${lane.title}"`, `Are you sure?`, () => {
      const cardsToRemove: number[] = [];
      for( const card of lane.cards ) {
        if( card.done ) cardsToRemove.push( card.id );
      }
      this.cardsService.deleteCards( cardsToRemove );
    })
  }

  editLane( lane: CardLane ) {
    const dialogRef = this.dialog.open<EditLaneDialogComponent, EditLaneDialogData, EditLaneDialogResult>(EditLaneDialogComponent, {
      data: {
        dialogTitle: 'Edit lane properties',
        laneId: lane.id,
        title: lane.title,
      },
    });

    dialogRef.afterClosed().subscribe( result => {
      if( result ) {
        this.cardsService.updateLane( result.laneId, result.title )
        console.log('Line is edited', result );
      }
    });
  }

  newCard( lane: CardLane ) {
    const dialogRef = this.dialog.open<EditCardDialogComponent, CardEditDialogData, CardEditDialogResult>(EditCardDialogComponent, {
      data: {
        dialogTitle: 'New card',
        cardId: 0,
        text: 'new task thingie',
        cardTypes: this.cardsService.getCardTypes(),
        type: 'default',
      },
    });

    dialogRef.afterClosed().subscribe( result => {
      if( result ) {
        this.cardsService.newCard( lane.id, result.text, result.details, result.type );
        console.log('Card is added', result );
      }
    });
  }

  saveDb() {
    this.cardsService.pushToBackend();
  }

  loadDb() {
    this.cardsService.getFromBackend();
  }

  importData() {
    const dialogRef = this.dialog.open<ImportDialogComponent, ImportDialogData, ImportDialogResult>(ImportDialogComponent, {
      data: { text: 'some data' },
    });

    dialogRef.afterClosed().subscribe( result => {
      if( result ) {
        this.cardsService.import( result.collection );
        console.log('Collection is imported', result );
      }
    });
  }

  exportData() {
    let txtData = JSON.stringify( this.cardsService.export(), null, '  ' );
    const filename = 'export.json';
    let f = new File( [ new Blob([txtData], { type: "application/json" }) ], filename );
    let hobj = window.URL.createObjectURL( f );

    var c = document.createElement("a");
    c.download = filename;
    c.href = hobj;
    c.click();
    window.URL.revokeObjectURL( hobj );
    c.remove();
  }

  cardDrop( event: CdkDragDrop<CardLane> ) {
    if( event.previousContainer === event.container ) {
      if( event.previousIndex !== event.currentIndex )
        this.cardsService.moveCardInLane( event.container.data.id, event.previousIndex, event.currentIndex );
    } else {
      this.cardsService.moveCardBetweenLanes(
        event.previousContainer.data.id,
        event.container.data.id,
        event.previousIndex,
        event.currentIndex,
       );
    }
  }

  laneDrop( event: CdkDragDrop<any>) {
    // console.log('laneDrop',event)
    if( event.previousContainer === event.container &&
      event.previousIndex !== event.currentIndex ) {
      // console.log('moveLane: ',event.previousIndex,event.currentIndex,);
      this.cardsService.moveLane(
        event.previousIndex,
        event.currentIndex,
       );
    } else {
      // console.log('laneDrop: NOPE');
    }
  }
}
