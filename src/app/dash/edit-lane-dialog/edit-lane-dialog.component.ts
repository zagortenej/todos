import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface EditLaneDialogData {
  dialogTitle: string;
  laneId: number;
  title: string;
}

export interface EditLaneDialogResult {
  laneId: number;
  title: string;
}

@Component({
  selector: 'app-edit-lane-dialog',
  templateUrl: './edit-lane-dialog.component.html',
  styleUrls: ['./edit-lane-dialog.component.scss']
})
export class EditLaneDialogComponent {

  dialogTitle: string = '';

  result: EditLaneDialogResult = {
    laneId: 0,
    title: '',
  };

  constructor(
    public dialogRef: MatDialogRef<EditLaneDialogComponent, EditLaneDialogResult>,
    @Inject(MAT_DIALOG_DATA) data: EditLaneDialogData,
  ) {
    this.dialogTitle = data.dialogTitle;
    this.result = {
      laneId: data.laneId,
      title: data.title,
    };
  }

  onEnterKey() {
    this.dialogRef.close( this.result );
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
