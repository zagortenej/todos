import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLaneDialogComponent } from './edit-lane-dialog.component';

describe('EditLaneDialogComponent', () => {
  let component: EditLaneDialogComponent;
  let fixture: ComponentFixture<EditLaneDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditLaneDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditLaneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
