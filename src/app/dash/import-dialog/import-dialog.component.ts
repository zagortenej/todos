import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LaneSet } from 'src/app/service/card-data.model';
import { CardsService } from 'src/app/service/cards.service';

export interface ImportDialogData {
  text: string;
}

export interface ImportDialogResult {
  collection: LaneSet;
}

@Component({
  selector: 'app-import-dialog',
  templateUrl: './import-dialog.component.html',
  styleUrls: ['./import-dialog.component.scss']
})
export class ImportDialogComponent {

  canSubmit: boolean = false;
  message: string = '';

  result: ImportDialogResult = {
    collection: {
      nextId: 0,
      title: '',
      lanes: [],
    }
  };

  constructor(
    public dialogRef: MatDialogRef<ImportDialogComponent, ImportDialogResult>,
    @Inject(MAT_DIALOG_DATA) data: ImportDialogData,
  ) {}

  onFileSelected( content: string ) {
    try {
      const collection: LaneSet = JSON.parse( content );

      if( CardsService.isValidLaneSet( collection ) ) {
        this.result.collection = collection;
        this.canSubmit = true;
      } else {
        this.message = 'The file does not contain a valid card collection.';
        console.error( 'this is NOT valid CARD COLLECTION', collection );
      }

    } catch( ex ) {
      this.message = 'The file does not contain a valid JSON.';
      console.error( 'this is NOT valid JSON', content );
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
