import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-select-import-file',
  templateUrl: './select-import-file.component.html',
  styleUrls: ['./select-import-file.component.scss']
})
export class SelectImportFileComponent {

  @ViewChild('fileUpload') fileUpload!: ElementRef<HTMLInputElement>;

  // @Input()
  requiredFileType: string = 'application/json';
  // requiredFileType: string = '.json';

  @Output()
  fileSelected = new EventEmitter<string>();

  fileName: string = '';

  onFileSelected() {

    if( this.fileUpload.nativeElement.files ) {

      const file: File = this.fileUpload.nativeElement.files[0];
      if( file ) {
        this.fileName = file.name;
        file.text().then( content => {
          this.fileSelected.emit( content );
        })
      }
    }
  }
}
