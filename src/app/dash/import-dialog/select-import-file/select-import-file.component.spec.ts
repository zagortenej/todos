import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectImportFileComponent } from './select-import-file.component';

describe('SelectImportFileComponent', () => {
  let component: SelectImportFileComponent;
  let fixture: ComponentFixture<SelectImportFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectImportFileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectImportFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
