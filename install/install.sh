#!/bin/bash

# https://codefather.tech/blog/bash-get-script-directory/
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
LIBS_DIR="${SCRIPT_DIR}/../build/libs"

jars=(`ls ${LIBS_DIR}/todos-all-in-one-*.jar 2> /dev/null`)
[ -z ${jars} ] && echo "No jar archives found in ${LIBS_DIR}" && exit 1

latest_jar=${jars[-1]}
[ ! -f ${latest_jar} ] && echo "No jar archives found in ${LIBS_DIR}" && exit 1


if [ -d "$HOME/.local/bin" ] ; then
  INSTALL_PATH="$HOME/.local/bin"
fi

if [ -d "$HOME/bin" ] ; then
  INSTALL_PATH="$HOME/bin"
fi

[ ! -v INSTALL_PATH ] && INSTALL_PATH="$HOME/.local/bin"

set -e
echo "Will install '${latest_jar}' into '${INSTALL_PATH}'"
mkdir -p ${INSTALL_PATH}
cp -f "${SCRIPT_DIR}/todos" ${INSTALL_PATH}
chmod +x "${SCRIPT_DIR}/todos"
cp -f ${latest_jar} ${INSTALL_PATH}/todos.jar
echo "Done."
