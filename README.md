# Todos

Simple task tracking web app.

## build / dev

To do local development:

First terminal:
```
$ npm start
```
Second terminal:
```
$ gradle run
```
Backend service will run on localhost:8080, and frontend will be on localhost:4200

To build and package production distribution:
```
$ gradle package
```
The resulting Jar file is in `build/libs` folder.

To run packaged app:
```
$ java -jar todos-all-in-one-<version>.jar
```
It will start and load/save to `todos-db.json` from current folder.

To specify port and/or json file to use:
```
$ java -jar todos-all-in-one-<version>.jar --db my-db.json --port 8080
```
Same thing using Java System properties:
```
$ java -jar todos-all-in-one-<version>.jar -Dservice.db=my-db.json -Dservice.port=8080
```
